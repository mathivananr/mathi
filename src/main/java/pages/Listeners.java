package pages;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

public class Listeners implements ITestListener   
{  
	
	
public void onTestStart(ITestResult result) { 
	
	try {
		Reporter.log("<br> Method name is - "+result.getName()+ " ;" );
		System.out.println("Test Starting");
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}  
  
public void onTestSuccess(ITestResult result) { 
	
	try {
		Reporter.log("<br> Test success and Status of execution is "+result.getStatus()+ " ;" );
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}  
  
public void onTestFailure(ITestResult result) {
	
	try {
		TestPageHandler test = new TestPageHandler();
		test.quitWebDriver();
		TestPageHandler.takeSnapShot("Test failed");
		Reporter.log("<br> Test failed and Status of execution is "+result.getStatus()+ " ;" );
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  
}  
  
public void onTestSkipped(ITestResult result) { 
	
	try {
		Reporter.log("<br> Test skipped and Status of execution is "+result.getStatus()+ " ;" );
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  
}

@Override
public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	// TODO Auto-generated method stub
	
}

@Override
public void onStart(ITestContext context) {
	// TODO Auto-generated method stub
	
}

@Override
public void onFinish(ITestContext context) {
	// TODO Auto-generated method stub
	
}  
}  
