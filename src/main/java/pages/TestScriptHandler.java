package pages;

import java.io.File;
import org.testng.log4testng.Logger;
import java.io.FileInputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class TestScriptHandler extends TestPageHandler {

	public static Logger logger;

	public TestScriptHandler() {

		logger = Logger.getLogger(TestScriptHandler.class);

	}

	@BeforeMethod(alwaysRun = true)
	public void setUp() {
		
		try{
		initializeWebDriver("chrome","grid");
		//maximize_Window_LaunchURL(getPropertyFile("URL_Edurekha"));
		//maximize_Window_LaunchURL(getPropertyFile("URL")); 
		} catch(Exception e) {
		logger.error("exception in 'start' method" +e.getMessage());
		e.printStackTrace(); 
		Assert.fail();
		}
		}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		try {
			quitWebDriver();
		} catch (Exception e) {
			logger.error("exception in 'end' method" + e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}
	}

	public static String getPropertyFile(String key) {

		String value = "";
		FileInputStream fis = null;
		Properties prop = null;
		try {
			String path = System.getProperty("user.dir") + "/src/test/resources/testdata/ClearTrip.Properties";
			File file = new File(path);
			fis = new FileInputStream(file);
			prop = new Properties();
			prop.load(fis);
			value = prop.getProperty(key);
		} catch (Exception e) {
			logger.error("exception in 'getPropertyFile' method" + e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}
		return value;
	}
}
