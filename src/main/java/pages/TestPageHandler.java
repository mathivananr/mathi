package pages;

import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.commons.io.FileUtils;


public class TestPageHandler {

	public static ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	public static WebDriverWait wait;
	public static String fileWithPath;
	public static int screenShotCount;

	public TestPageHandler() {
	}

	public WebDriver getDriver() {
		return TestPageHandler.driver.get();
	}

	public static void setDriver(WebDriver driver) {
		TestPageHandler.driver.set(driver);
	}

	public static void takeSnapShot(String message){
		try {
			screenShotCount+=1;
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			LocalDateTime now = LocalDateTime.now();  
			String date=dtf.format(now); 
			TakesScreenshot scrShot =((TakesScreenshot)driver);
			File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
			fileWithPath=System.getProperty("user.dir")+"/src/test//resources//screenshot/"+date+"_"+screenShotCount+".png";
			File DestFile=new File(fileWithPath);
			FileUtils.copyFile(SrcFile, DestFile);
			Reporter.log(message);
			Reporter.log("<br> <a href='"+DestFile.getAbsolutePath()+"'> <img src='"+ DestFile.getAbsolutePath()+"' height='200' width='200'/> </a>  ");
		}catch(Exception e) {
			Reporter.log("exception in 'takeSnapShot' method " +e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void initializeWebDriver(String browser,String localorgrid) {
		try {
			if(browser.contentEquals("chrome")) {
				
				if(localorgrid.contentEquals("grid")) {
					DesiredCapabilities capability = DesiredCapabilities.chrome();
					//setDriver(new RemoteWebDriver(new URL("http://3.137.163.85:4444"), capability));
					setDriver(new RemoteWebDriver(new URL("http://selenium-hub-selenium-infra.apps.poc-12345.rifv.p1.openshiftapps.com/wd/hub"), capability));
					Reporter.log(" 'Chrome' browser launched");
				}

				if(localorgrid.contentEquals("local")) {
					//WebDriverManager.chromedriver().setup();
					System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+TestScriptHandler.getPropertyFile("chromeDriverPath"));
					setDriver(new ChromeDriver());
					Reporter.log("'Chrome' browser launched");
				}

			}else if (browser.contentEquals("internetexplorer")) {
				if(localorgrid.contentEquals("grid")) {
					DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
					setDriver(new RemoteWebDriver(new URL("http://3.137.163.85:4444"), capability));
					Reporter.log("'Internet Explore' browser launched");
				}
				if(localorgrid.contentEquals("local")) {
					WebDriverManager.chromedriver().setup();
					//System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+TestScriptHandler.getPropertyFile("ieDriverPath"));
					setDriver( new InternetExplorerDriver());
					Reporter.log(" 'Internet Explore' browser launched");
				}
			}
			else {
				Reporter.log("Browser name not matching");
			}
			wait = new WebDriverWait(getDriver(), 60);
		}catch(Exception e) {
			Reporter.log("exception in 'initializeWebDriver' method " +e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void quitWebDriver() {
		try {
			getDriver().quit();
		}catch(Exception e) {
			Reporter.log("exception in 'quitWebDriver' method"  +e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void maximize_Window_LaunchURL(String url) {
		try {
			// To maximize browser  
			getDriver().manage().window().maximize();

			Reporter.log("window maximized");
			// Implicit wait
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
			// To launch URL
			getDriver().get(url);

			Reporter.log("URL : '"+url+"' launched successfully");
		}catch(Exception e) {
			Reporter.log("exception in 'maximize_Window_LaunchURL' method " +e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}
	}
}
