package pages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

public class ClearTrip_Page extends TestPageHandler{

	public static By tag_ClearTrip =By.xpath("//a[@data-test-attrib='cleartrip-logo']");
	public static By radioButton_RoundTrip =By.xpath("//p[contains(text(),'Round trip')]//parent::div/parent::label/div/span");
	public static By button_SearchFlights =By.xpath("//div[@class='row pt-3 pb-4 p-relative px-4']/div[@class='col flex flex-middle']/button");
	public static By textBox_From =By.xpath("//h4[contains(text(),'From')]/parent::div/div/div/div/input");
	public static By textBox_To =By.xpath("//h4[contains(text(),'To')]/parent::div/div/div/div/input");
	public static By dropDown_From_To;
	public static By dropDown_DepartOn =By.xpath("//h4[contains(text(),'Depart on')]/parent::div/parent::div/following-sibling::div//button[1]");
	public static By dropDown_ReturnOn =By.xpath("//h4[contains(text(),'Return on')]/parent::div/parent::div/following-sibling::div//button[2]");
	public static By button_DepartOn =By.xpath("//h4[contains(text(),'Depart on')]/parent::div/parent::div/parent::div//button[1]");
	public static By button_ReturnOn =By.xpath("//h4[contains(text(),'Depart on')]/parent::div/parent::div/parent::div//button[2]");
	public static By button_book =By.xpath("//button[contains(text(),'Book')]");
	
	public static By value_DepartOn =By.xpath("");
	public static By value_ReturnOn =By.xpath("");


	public static By dropdown_Adults =By.xpath("//h4[contains(text(),'Adults')]/parent::div/select");
	public static By option_Adults =By.xpath("");
	
	public static By dropdown_Children =By.xpath("//h4[contains(text(),'Children')]/parent::div/select");
	public static By option_Children =By.xpath("");
	
	public static By dropdown_Infants =By.xpath("//h4[contains(text(),'Infants')]/parent::div/select");
	public static By option_Infants =By.xpath("");



	public ClearTrip_Page() {

	}

	public void verify_Page_ClearTrip() {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(tag_ClearTrip));
			if(getDriver().findElement(tag_ClearTrip).isDisplayed()) {
				takeSnapShot("<br> Navigated to 'clear trip home page' successfully");
			}
		} catch (Exception e) {

			
			Reporter.log("exception in 'verify_Page_ClearTrip' method " +e.getMessage());
			takeSnapShot("exception in 'verify_Page_ClearTrip' method ");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	public void click_Button_Book() {
		try {
			//wait.until(ExpectedConditions.visibilityOfElementLocated(button_book));
			//if(getDriver().findElement(button_book).isDisplayed()) {
				//getDriver().findElement(button_book).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Airlines')]")));
			
				takeSnapShot("<br> Book page displayed");
			//}
		} catch (Exception e) {
			Reporter.log("exception in 'click_Button_Book' method " +e.getMessage());
			takeSnapShot("exception in 'click_Button_Book' method ");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	

	public void click_RadioButton_RoundTrip() {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(radioButton_RoundTrip));
			if(getDriver().findElement(radioButton_RoundTrip).isDisplayed()) {
				getDriver().findElement(radioButton_RoundTrip).click();
				takeSnapShot("<br> Round Trip Radio Button Selected");
			}
		} catch (Exception e) {

			
			Reporter.log("exception in 'click_RadioButton_RoundTrip' method " +e.getMessage());
			takeSnapShot("exception in 'click_RadioButton_RoundTrip' method ");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	public void click_SearchFlight_Button() {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(button_SearchFlights));
			if(getDriver().findElement(button_SearchFlights).isDisplayed()) {
				getDriver().findElement(button_SearchFlights).click();
				takeSnapShot("<br> Search Flight Button Clicked");
			}
		} catch (Exception e) {

			Reporter.log("exception in 'click_SearchFlight_Button' method " +e.getMessage());
			takeSnapShot("exception in 'click_SearchFlight_Button' method ");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	public void select_DropDown_From(String from) {
		try {
			dropDown_From_To=By.xpath("//p[contains(text(),'"+from+"')]");
			wait.until(ExpectedConditions.visibilityOfElementLocated(textBox_From));
			if(getDriver().findElement(textBox_From).isDisplayed()) {
				getDriver().findElement(textBox_From).click();
				takeSnapShot("<br> Clicked on From textbox");
				wait.until(ExpectedConditions.visibilityOfElementLocated(dropDown_From_To));
				if(getDriver().findElement(dropDown_From_To).isDisplayed()) {
					getDriver().findElement(dropDown_From_To).click();
					takeSnapShot("<br> Selected "+from+" value from From Destination");
				}
			}
		} catch (Exception e) {
			
			Reporter.log("exception in 'select_DropDown_From' method " +e.getMessage());
			takeSnapShot("exception in 'select_DropDown_From' method ");
			e.printStackTrace();
			Assert.fail();
		}
	}

	public void select_DropDown_To(String from) {
		try {
			dropDown_From_To=By.xpath("//p[contains(text(),'"+from+"')]");
			wait.until(ExpectedConditions.visibilityOfElementLocated(textBox_To));
			if(getDriver().findElement(textBox_To).isDisplayed()) {
				getDriver().findElement(textBox_To).click();
				takeSnapShot("<br> Clicked on To textbox");
				wait.until(ExpectedConditions.visibilityOfElementLocated(dropDown_From_To));
				if(getDriver().findElement(dropDown_From_To).isDisplayed()) {
					getDriver().findElement(dropDown_From_To).click();
					takeSnapShot("<br> Selected "+from+" value from To Destination");
				}
			}
		} catch (Exception e) {
			
			Reporter.log("exception in 'select_DropDown_To' method " +e.getMessage());
			takeSnapShot("exception in 'select_DropDown_To' method ");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	public  void genericDatePicker_Date(String inputDate,String departorreturn){
		
		String month="";
		String year="";
		
		if(departorreturn.contentEquals("Depart")) {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(button_DepartOn));
		WebElement ele =getDriver().findElement(button_DepartOn);  
		ele.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='DayPicker-Caption'])[1]/div")));
		 month = getDriver().findElement(By.xpath("(//div[@class='DayPicker-Caption'])[1]/div")).getText().split(" ")[0];
		 year = getDriver().findElement(By.xpath("(//div[@class='DayPicker-Caption'])[1]/div")).getText().split(" ")[1];
		
		}
		
		if(departorreturn.contentEquals("Return")) {
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(button_ReturnOn));
			WebElement ele =getDriver().findElement(button_ReturnOn);  
			ele.click();
			ele.click();
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='DayPicker-Caption'])[2]/div")));
			 month = getDriver().findElement(By.xpath("(//div[@class='DayPicker-Caption'])[2]/div")).getText().split(" ")[0];
			 year = getDriver().findElement(By.xpath("(//div[@class='DayPicker-Caption'])[2]/div")).getText().split(" ")[1];
			
			}
		
		
		System.out.println("Application month : "+month + " Year :"+year);
		int monthNum = getMonthNum(month);
		System.out.println("Enum Num : "+monthNum);
		String[] parts = inputDate.split("/");  
		int noOfHits = ((Integer.parseInt(parts[2])-Integer.parseInt(year))*12)+(Integer.parseInt(parts[1])-monthNum);
		System.out.println("No OF Hits "+noOfHits);
		for(int i=0; i< noOfHits;i++){
		getDriver().findElement(By.className("nextMonth ")).click();
		}
		List<WebElement> cals=getDriver().findElements(By.xpath("//div[@class='monthBlock first']//tr"));
		System.out.println(cals.size());
		for( WebElement daterow : cals){
		List<WebElement> datenums = daterow.findElements(By.xpath("//td"));
		for(WebElement date : datenums ){
		if(date.getText().equalsIgnoreCase(parts[0])){
		date.click();
		break;
		}
		}
		}
		}

		public  int getMonthNum(String month){
		for (Month mName : Month.values()) {
		if(mName.name().equalsIgnoreCase(month))
		return mName.value;
		}
		return -1;
		}

		public enum Month {
		January(1), February(2), March(3), April(4), May(5), June(6) , July(7), August(8), September(9), October(10), November(11),December(12);
		private int value;

		private Month(int value) {
		this.value = value;
		}
		}


	public void select_DropDown_DepartOn_ReturnOn() {
		try {

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd");  
			LocalDateTime now = LocalDateTime.now().plusDays(2);  
			String date=dtf.format(now);
			LocalDateTime nowplustwo= LocalDateTime.now().plusDays(4);
			  String datefuture=dtf.format(nowplustwo);
			wait.until(ExpectedConditions.visibilityOfElementLocated(button_DepartOn));
			if(getDriver().findElement(button_DepartOn).isDisplayed()) {
				getDriver().findElement(button_DepartOn).click();
				takeSnapShot("<br> Clicked on Depart On button");
				value_DepartOn=By.xpath("(//div[@class='Day-grid flex flex-middle flex-column flex-center']//div[contains(text(),'"+date+"')])[1]");
				wait.until(ExpectedConditions.visibilityOfElementLocated(value_DepartOn));
				WebElement element = getDriver().findElement(value_DepartOn);
				Actions actions = new Actions(getDriver());
				actions.moveToElement(element);
				actions.perform();
				if(getDriver().findElement(value_DepartOn).isDisplayed()) {
					getDriver().findElement(value_DepartOn).click();
					takeSnapShot("<br> "+date+" has been selected from the depart On date");
				}
				takeSnapShot("<br> Clicked on Return On button"); value_ReturnOn=By.xpath("(//div[@class='Day-grid flex flex-middle flex-column flex-center']//div[contains(text(),'"+datefuture+"')])[2]");
						  wait.until(ExpectedConditions.visibilityOfElementLocated(value_ReturnOn));
						  WebElement element1 = getDriver().findElement(value_DepartOn);
							Actions actions1 = new Actions(getDriver());
							actions1.moveToElement(element1);
							actions1.perform();
						  if(getDriver().findElement(value_ReturnOn).isDisplayed()) {
						  getDriver().findElement(value_ReturnOn).click();
						  takeSnapShot("<br> "+datefuture+" has been selected from the Return On date"); } 
			}
		}catch(Exception e) {

			
			Reporter.log("exception in 'select_DropDown_DepartOn' method " +e.getMessage());
			takeSnapShot("exception in 'select_DropDown_DepartOn' method ");
			e.printStackTrace();
			Assert.fail();

		}
	}

	
	public void select_DropDown_Adults(String value) {
		try {

			wait.until(ExpectedConditions.visibilityOfElementLocated(dropdown_Adults));
			if(getDriver().findElement(dropdown_Adults).isDisplayed()) {
				getDriver().findElement(dropdown_Adults).click();
				option_Adults=By.xpath("//h4[contains(text(),'Adults')]/parent::div/select/option[contains(text(),'"+value+"')]");
				wait.until(ExpectedConditions.visibilityOfElementLocated(option_Adults));
				if(getDriver().findElement(option_Adults).isDisplayed()) {
					getDriver().findElement(option_Adults).click();
					takeSnapShot("<br> "+value+" has been selected from adult dropdown");
				}
			}
		}catch(Exception e) {

		
			Reporter.log("exception in 'select_DropDown_Adults' method " +e.getMessage());
			takeSnapShot("exception in 'select_DropDown_Adults' method ");
			e.printStackTrace();
			Assert.fail();

		}

	}
	
	public void select_DropDown_Children(String value) {
		try {

			wait.until(ExpectedConditions.visibilityOfElementLocated(dropdown_Adults));
			if(getDriver().findElement(dropdown_Adults).isDisplayed()) {
				getDriver().findElement(dropdown_Adults).click();
				option_Children=By.xpath("//h4[contains(text(),'Children')]/parent::div/select/option[contains(text(),'"+value+"')]");
				wait.until(ExpectedConditions.visibilityOfElementLocated(option_Children));
				if(getDriver().findElement(option_Children).isDisplayed()) {
					getDriver().findElement(option_Children).click();
					takeSnapShot("<br> "+value+" has been selected from adult dropdown");
				}
			}
		}catch(Exception e) {
			
			
			Reporter.log("exception in 'select_DropDown_Children' method " +e.getMessage());
			takeSnapShot("exception in 'select_DropDown_Children' method ");
			e.printStackTrace();
			Assert.fail();

		}

	}
	
	public void select_DropDown_Infants(String value) {
		try {

			wait.until(ExpectedConditions.visibilityOfElementLocated(dropdown_Infants));
			if(getDriver().findElement(dropdown_Infants).isDisplayed()) {
				getDriver().findElement(dropdown_Infants).click();
				option_Infants=By.xpath("//h4[contains(text(),'Adults')]/parent::div/select/option[contains(text(),'"+value+"')]");
				wait.until(ExpectedConditions.visibilityOfElementLocated(option_Infants));
				if(getDriver().findElement(option_Infants).isDisplayed()) {
					getDriver().findElement(option_Infants).click();
					takeSnapShot("<br> "+value+" has been selected from Infant dropdown");
				}
			}
		}catch(Exception e) {
			
			Reporter.log("exception in 'select_DropDown_Infants' method " +e.getMessage());
			takeSnapShot("exception in 'select_DropDown_Infants' method ");
			e.printStackTrace();
			Assert.fail();
		}
	}
}
