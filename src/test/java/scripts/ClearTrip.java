package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.ClearTrip_Page;
import pages.TestScriptHandler;

@Listeners(pages.Listeners.class)
public class ClearTrip extends TestScriptHandler {

	ClearTrip_Page ClearTrip = new ClearTrip_Page();

	@Test()
	public void averify_Create_Round_Trip() {
		try {
			for(int i=0;i<2;i++){
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
			Thread.sleep(10000);
			
			}
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void btestCase2() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void ctestCase4() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void dtestCase3() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void etestCase5() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void ftestCase6() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void gtestCase7() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void htestCase8() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test()
	public void itestCase9() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Test()
	public void jtestCase10() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Test()
	public void ktestCase11() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Test()
	public void ltestCase12() {
		try {
			getDriver().get("http://www.google.com");
			(new WebDriverWait(getDriver(), 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			WebElement element = getDriver().findElement(By.name("q"));
			element.sendKeys("Java");
			element.submit();
			System.out.println("Thread ID " + Thread.currentThread().getId());
			Thread.sleep(10000);
			WebElement element1 = getDriver().findElement(By.name("q"));
			element1.click();
			element1.clear();
			element1.sendKeys("selenium");
			element1.submit();
			Thread.sleep(10000);
			WebElement element2 = getDriver().findElement(By.name("q"));
			element2.click();
			element2.clear();
			element2.sendKeys("Cucubmber");
			element2.submit();
			Thread.sleep(10000);
			WebElement element3 = getDriver().findElement(By.name("q"));
			element3.click();
			element3.clear();
			element3.sendKeys("Rest Assured");
			element3.submit();
			Thread.sleep(10000);
			WebElement element4 = getDriver().findElement(By.name("q"));
			element4.click();
			element4.clear();
			element4.sendKeys("Jenkins");
			element4.submit();
		} catch (Exception e) {
			Reporter.log("Error in method" + e.getMessage());
			e.printStackTrace();
		}
	}
}